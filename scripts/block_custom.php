#!/usr/bin/env drush
<?php
/**
 * @file
 * Drush script to take care of creating custom blocks.
 */

// Make sure our helper module is enabled.
if (!module_exists('headstart_block')) {
  module_enable(array('headstart_block'));
}

// Our array of blocks. Because we have no way of tracking which block
// is which, we give each one an arbitrary machine name, which can be
// anything. Just don't change it. Example:

$blocks = array(
  'my_machine_name' => array(
    'info' => 'My Administrative Title',
    'title' => 'My User-facing Title',
    'body' => '<p>My block body.</p>',
    'format' => 'full_html', // default = 'filtered_html'
    'theme' => 'my_theme', // defaults to default theme
  ),
);
headstart_block_save_custom_blocks($blocks);

