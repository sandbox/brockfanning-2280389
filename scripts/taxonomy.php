#!/usr/bin/env drush
<?php
/**
 * @file
 * Drush script to take care of creating/updating taxonomy terms/vocabularies.
 */

// Make sure our helper module is enabled.
if (!module_exists('headstart_taxonomy')) {
  module_enable(array('headstart_taxonomy'));
}

// Our array of vocabularies/terms. Because we have no way of tracking which
// is which, we give each one an arbitrary machine name, which can be
// anything. Just don't change it. 
// 
// Vocabularies: Possible options and their defaults are:
// 'description' => '',
// 'hierarchy' => 1,
// 
// Terms: Possible options and their defaults are:
// 'description' => '',
// 'format' => 'filtered_html',

$vocabularies = array(
  'machine_name|Human-Readable Name' => array(


  ),
);