#!/usr/bin/env drush
<?php
/**
 * @file
 * Drush script to take care of placing blocks.
 */

// Make sure our helper module is enabled.
if (!module_exists('headstart_block')) {
  module_enable(array('headstart_block'));
}

// This is a large nested array of blocks, grouped by region. Within each
// region, the blocks can be listed with the module|delta only, or the
// module|delta can be a key pointing to an array of options like: weight,
// pages, etc. 
// - If weight is omitted, it will be incremented by 2 for each block.
// - If the module is 'block', the delta is assumed to be arbitrary
//   "machine name" that we chose in block_custom.php.
$blocks_by_region = array(
  // Place some blocks in the "content" region.
  'content' => array(
    // First a custom block we created in block_custom.php
    'block|my_machine_name' => array(
      // We only want it on the front page.
      'visibility' => BLOCK_VISIBILITY_LISTED,
      'pages' => '<front>',
    ),
    // Next the system main block, which we just want everywhere.
    'system|main',
  ),
);

headstart_block_place_blocks($blocks_by_region);
