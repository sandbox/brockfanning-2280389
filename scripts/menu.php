#!/usr/bin/env drush
<?php
/**
 * @file
 * Drush script to take care of creating menus and links.
 */

// Make sure our helper module is enabled.
if (!module_exists('headstart_menu')) {
  module_enable(array('headstart_menu'));
}

/**
 * The stucture of the arrays of links below is like so:
 * 'link title|link path',
 * 'link title|link path',
 * 'link title|link path' => array(
 *   'link title|link path',
 *   'link title|link path',
 *  ),
 *  
 * Any links with children will be set to "expanded" automatically.
 * (There are cases where this is probably not desired, but personally I have
 *  not encountered any.)
 *
 * Note that this script wipes the menu links and creates them anew, so
 * menu link ID's will change.
 */

// The main menu
$menu_name = 'my-menu';
$title = 'My Menu';
$description = 'Description of my example menu.';
$links = array(
  'My Parent Link Title|url/for/my/parent/link' => array(
    'My Child Link|url/for/my/parent/link/child',
  ),
);
headstart_menu_create_menu($menu_name, $title, $description, $links);