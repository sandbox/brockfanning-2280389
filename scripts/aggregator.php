#!/usr/bin/env drush
<?php
/**
 * @file
 * Drush script to take care of creating aggregator feeds/categories.
 */

// Make sure our helper module is enabled.
if (!module_exists('headstart_aggregator')) {
  module_enable(array('headstart_aggregator'));
}

// First the categories. Because we have no way of tracking the
// categories, we need an arbitrary machine name for each one. It can
// be anything, just don't change it. Note: Categories are optional.
$categories = array(
  'my_category_machine_name' => array(
    'title' => 'My Category Name',
    'description' => '',
    'block' => 3,
  ),
);
headstart_aggregator_save_categories($categories);

// Next the feeds themselves. Similarly, we give each one an arbitrary
// machine name. Possible options and their defaults are:
// 'refresh' => 3600,     (ie, number of seconds before refreshing items)
// 'block' => 5,          (ie, number of items to display in its block)
// Also note: 'categories' can be an array.
$feeds = array(
  'my_feed_machine_name' => array(
    'title' => 'My Feed Title',
    'url' => 'http://example.com/feed.rss',
  ),
);
headstart_aggregator_save_feeds($feeds);

