#!/usr/bin/env drush
<?php
/**
 * @file
 * Drush script to take care of creating/updating roles/permissions.
 */

// Make sure our helper module is enabled.
if (!module_exists('headstart_role')) {
  module_enable(array('headstart_role'));
}

/* Our array of roles/permissions, in the format of:
 * array(
 *   'role name' => array(
 *     'permission1',
 *     'permission2',
 *     'etc',
 *   ),
 * )
 * One special case...
 * 'role name' => 'ALL'
 * ...will grant all permissions to that role.
 */

$roles = array(
  'admin' => 'ALL',
  'editor' => array(
    'access administration menu',
    'access administration pages',
    'access content overview',
    'administer nodes',
  )
);

headstart_role_create_roles($roles);