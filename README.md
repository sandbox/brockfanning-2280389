<h1>Headstart</h1>

<h2>Overview</h2>

The Headstart suite of modules is intended to aid in the scripted creation of Drupal content, during development. The expectation is that when needed, developers will clone the project, and tweak the scripts (scripts/*) as needed for that project. These scripts aid development by allowing you to create and update starter content (like blocks, menus, pages, etc) in code and from the command line.

<h2>Usage</h2>

Tweak the included scripts as needed. For ease of use these are all located in the same directory: `scripts`.

Examples:
* block_custom.php: create/update custom blocks
* block_placement.php: control block placement and visibility
* menu.php: create/update custom menus and menu links
* aggregator.php: create/update aggregator feeds 

Run the scripts as needed, using Drush. For example:

* `drush scr block_custom.php`
* `drush scr block_placement.php`

Feel free to move the scripts to other places. However if moved above the document root, you would need to use Drush aliases to run them.

Extra bonus: All scripts are designed to be re-runnable. So for example, if you need to change to a menu link path, or re-order some blocks, you can make the change in the drush script, and then run it as normal, and the updates will happen in place.

Update functions: You can also use the code in the scripts as a guide for how to do similar things in update functions. 

<h2>Documentation</h2>

See the scripts themselves for documentation on tweaking them.